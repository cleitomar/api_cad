@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{ url('/cliente') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col col-lg-8">
                <div class="form-group">
                    <input type="text" class="form-control" name="nome"  placeholder="Enter Nome">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="email"  placeholder="Enter Email">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="logradouro" placeholder="Enter Logradouro">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="numero" placeholder="Enter Numero">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="complemento" placeholder="Enter Complemento">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="cep" placeholder="Enter Cep">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="bairro" placeholder="Enter Bairro">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="cidade" placeholder="Enter Cidade">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="uf" placeholder="Enter UF">
                </div>
            </div>
        </div>
        
    
        <button type="submit" class="btn btn-primary">Enviar</button>
      </form>
</div>
@endsection
