@extends('layouts.app')

@section('content')
<div class="container">
<a href="{{ url("/novo") }}" class="btn btn-primary mb-4">Adicionar novo</a>

    <table class="table">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Nome</th>
            <th scope="col">Email</th>
            <th scope="col">Logradouro</th>
            <th scope="col">Numero</th>
            <th scope="col">Complemento</th>
            <th scope="col">Cep</th>
            <th scope="col">Bairro</th>
            <th scope="col">Cidade</th>
            <th scope="col">UF</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($clientes as $cliente)
                <tr>
                    <th>{{ $cliente->id }}</th>
                    <th>{{ $cliente->nome }}</th>
                    <th>{{ $cliente->email }}</th>
                    <th>{{ $cliente->logradouro }}</th>
                    <th>{{ $cliente->numero }}</th>
                    <th>{{ $cliente->complemento }}</th>
                    <th>{{ $cliente->cep }}</th>
                    <th>{{ $cliente->bairro }}</th>
                    <th>{{ $cliente->cidade }}</th>
                    <th>{{ $cliente->uf }}</th>                
                </tr>
            @empty
                <h2>Nenhum cadastro</h2>
            @endforelse
                   
        </tbody>
      </table>
</div>
@endsection
